// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import {router} from './router'
import ('../node_modules/vuetify/dist/vuetify.min.css')
import socketio from 'socket.io-client'
import VueSocketio from 'vue-socket.io'
import axios from 'axios'

let BACKEND_URL = 'http://localhost:3000/'
const SocketInstance = socketio(BACKEND_URL)

Vue.prototype.$backend = axios.create({
  baseURL: BACKEND_URL,
  withCredentials:true
});

Vue.use(VueSocketio, SocketInstance)
Vue.use(Vuetify)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
