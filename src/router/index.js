import Vue from 'vue'
import Router from 'vue-router'
import groups from '@/components/groups'
import landingPage from '@/components/landingPage'
import userProfile from '@/components/userProfile'
import auth from '../services/auth'

Vue.use(Router)


let routes = [{
    path: '/',
    name: 'landingPage',
    component: landingPage
  },
  {
    path: '/group',
    name: 'groups',
    component: groups
  },
  {
    path: '/users/:user',
    name: 'userprofile',
    component: userProfile,
    meta: {
      requiresAuth: true
    }
  }
]


export const router = new Router({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  // to and from are both route objects
  if (to.matched.some(routes => routes.meta.requiresAuth)) {
    if (!auth.checkAuth(to.params.user)) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

