import axios from 'axios'
import {
  router
} from '../router/index'

let BASE_URL = 'http://localhost:3000'
let auth = axios.create({
  baseURL: BASE_URL,
  withCredentials:true
})

const LOGIN_URL = '/auth/local'

const SIGNUP_URL = '/signup/local'

const LOGOUT_URL = '/logout'

export default ({
  user: {
    authenticated: false
  },

  checkAuth(user){
      var token = localStorage.getItem('token')
      if(token === user){
          this.user.authenticated = true
          return true
      }
      else{
          this.user.authenticated = false
          return false
      }
  },
  signup(context, creds){
    auth.post(SIGNUP_URL, creds).then(response => {
      console.log(response)
      router.push({name:'landingPage'})
    })
  }, 
  auth,
  logout(){
    auth.get(LOGOUT_URL).then(response => {
    localStorage.removeItem('token')
    this.user.authenticated = false
    console.log(response.status)  //Printing the status here
    router.push({
      name:'landingPage'
    })
  })
}
})

